/**
 * Exemplo3: Programacao com threads
 * Autor: Luana Belusso
 * Ultima modificacao: 01/04/2018 17:40
 */

package atividade3;

public class Exemplo3  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");
             
            //Cria cada thread com um novo runnable selecionado
            Thread t1 = new Thread(new PrintTasks("thread1", "verde"));            
            Thread t2 = new Thread(new PrintTasks("thread2", "amarelo")); 
            Thread t3 = new Thread(new PrintTasks("thread3", "vermelho")); 
            
            //Inicia as threads, e as coloca no estado EXECUTAVEL
            t1.start(); //invoca o método run de t1
            t2.start(); //invoca o método run de t2
            t3.start(); //invoca o método run de t3
            
            System.out.println("Threads criadas");
        }
        
}
