/**
 * Exemplo1: Programacao com threads
* Autor: Luana Belusso
* Ultima modificacao: 01/04/2018 17:40
 */
package atividade3;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PrintTasks implements Runnable {

    private final String taskName; 
    private String cor;
    private String hora;
    
    public PrintTasks(String name, String cor){
        this.taskName = name;
        this.cor = cor;      
    }
    
    public void run(){
        //while(true){
            switch(cor){
                case "verde":
                    this.hora = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                    this.cor = "amarelo";
                    System.out.printf("Nome: %s Cor:%s Hora:%s\n", this.taskName, this.cor, this.hora);
                    break;
                case "amarelo":
                    this.hora = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                    this.cor = "vermelho";
                    System.out.printf("Nome: %s Cor:%s Hora:%s\n", this.taskName, this.cor, this.hora);
                    break;
                case "vermelho":
                    this.hora = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
                    this.cor = "verde";
                    System.out.printf("Nome: %s Cor:%s Hora:%s\n", this.taskName, this.cor, this.hora);
                    break;
                default:
                    break;
            }
        //}   
    }
       
}
